#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>

int main() {
  std::cout << "Welcome to Learning Vulkan Engine" << std::endl;

  return 0;
}
